package com.lexcorp.bpbank.controllers;

import java.util.ArrayList;
import com.lexcorp.bpbank.models.PersonaModel;
import com.lexcorp.bpbank.services.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/personas")
public class PersonaController {
    @Autowired
    PersonaService personaService;

    @GetMapping()
    public ArrayList<PersonaModel> obtenerPersonas(){
        return personaService.obtenerPersonas();
    }

    @PostMapping
    public PersonaModel guardarPersona(@RequestBody PersonaModel persona){
        return this.personaService.guardarPersona(persona);
    }

}
