package com.lexcorp.bpbank.services;
import java.util.ArrayList;

import com.lexcorp.bpbank.models.PersonaModel;
import com.lexcorp.bpbank.repositories.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class PersonaService {
    @Autowired
    PersonaRepository personaRepository;

    public ArrayList<PersonaModel> obtenerPersonas(){

        return (ArrayList<PersonaModel>) personaRepository.findAll();
    }

    public PersonaModel guardarPersona(PersonaModel persona){
        return personaRepository.save(persona);
    }

}
