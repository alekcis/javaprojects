package com.lexcorp.bpbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BpbankApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpbankApplication.class, args);
	}

}
