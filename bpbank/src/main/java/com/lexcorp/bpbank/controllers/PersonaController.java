package com.lexcorp.bpbank.controllers;

import java.util.ArrayList;
import com.lexcorp.bpbank.models.PersonaModel;
import com.lexcorp.bpbank.services.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/personas")
public class PersonaController {
    @Autowired
    PersonaService personaService;

    @GetMapping()
    public ArrayList<PersonaModel> obtenerPersonas(){
        return personaService.obtenerPersonas();
    }

    @PostMapping
    public PersonaModel guardarPersona(@RequestBody PersonaModel persona){
        return this.personaService.guardarPersona(persona);
    }
    
    @GetMapping("/query")
    public ArrayList<PersonaModel> obtenerPersonaPorNombre(@RequestParam("nombre") String nombre){
    	return this.personaService.obtenerPorNombre(nombre);
    	
    } 
    
    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") Long id) {
    	boolean ok = this.personaService.eliminarPersona(id);
    	if (ok) {
			return "Se elimino el usuario con id: "+id;
		} else {
			return "No se pudo eliminar el usuario con id: "+id;
		}
    }

}