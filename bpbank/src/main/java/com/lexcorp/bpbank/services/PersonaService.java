package com.lexcorp.bpbank.services;
import java.util.ArrayList;
import java.util.Optional;

import com.lexcorp.bpbank.models.PersonaModel;
import com.lexcorp.bpbank.repositories.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class PersonaService {
    @Autowired
    PersonaRepository personaRepository;

    public ArrayList<PersonaModel> obtenerPersonas(){

        return (ArrayList<PersonaModel>) personaRepository.findAll();
    }

    public PersonaModel guardarPersona(PersonaModel persona){
        return personaRepository.save(persona);
    }
    
    public ArrayList<PersonaModel> obtenerPorNombre(String nombre){
    	return personaRepository.findBynombre(nombre);
    }
    
    public boolean eliminarPersona(Long id) {
    	try {
			personaRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
    }

}
